package com.ed.customer;

import com.ed.clients.fraud.FraudCheckResponse;
import com.ed.clients.fraud.FraudClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public record CustomerService(
        CustomerRepository customerRepository,
        RestTemplate restTemplate,
        FraudClient fraudClient) {

    public void registerCustomer(CustomerRegistrationRequest request) {
        var newCustomer = Customer.builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .build();
        // todo: check if email is valid
        // todo: check if email not taken
        // todo: check if fraudster
        customerRepository.saveAndFlush(newCustomer);
        FraudCheckResponse fraudCheckResponse =
                fraudClient.isFraud(newCustomer.getId());
//        FraudCheckResponse fraudCheckResponse = restTemplate.getForObject(
//                "http://FRAUD:8081/api/v1/fraud-check/{customerId}",
//                FraudCheckResponse.class,
//                newCustomer.getId());
        if (fraudCheckResponse.isFraudster())
            throw new IllegalStateException("fraudster");
        // todo: send notification
    }
}
