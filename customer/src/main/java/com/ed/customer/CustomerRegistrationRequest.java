package com.ed.customer;


public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email) {
}
